var ajaxurl = '<?php echo admin_url( "admin-ajax.php" ); ?>';

path = path.substring(0,path.lastIndexOf("\\")+1);
jQuery(document).ready(function($) {
    $('.form--contact').on('submit', function(e) {
        e.preventDefault();

        let formData = $(this).serializeArray();;
        let ajaxUrl = 'http://localhost/sprivaten/wp-admin/admin-ajax.php';
        let errors = [];

        formData.forEach(function(field) {
            if (field.name === 'email' && field.value.trim() !== '') {
                if (!isValidEmail(field.value)) {
                    errors.push('Invalid email address.');
                }
            } else if (field.value.trim() === '') {
                errors.push(field.name + ' is required.');
            }
        });

        if (errors.length > 0) {
            $('.custom-form-message').addClass('error-message')
            $('.custom-form-message').html(errors.join('<br>'));
        } else {
            $.ajax({
                type: 'POST',
                url: ajaxUrl,
                data: formData + '&action=custom_form_plugin_save_data',
                success: function (response) {
                    $('.custom-form-message').addClass('success-message')
                    $('.custom-form-message').html('Form submitted successfully.');
                },
                error: function (response) {
                    $('.custom-form-message').addClass('error-message')
                    $('.custom-form-message').html('Error saving form data!');
                }
            });
        }
    });
});
function isValidEmail(email) {
    // Regular expression for email validation
    var emailPattern = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
    return emailPattern.test(email);
}