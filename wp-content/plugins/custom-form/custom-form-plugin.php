<?php
/*
Plugin Name: Custom Form Plugin
Description: A plugin to handle custom form submissions.
Version: 1.0
Author: Your Name
*/
function custom_form_plugin_enqueue_scripts()
{
    wp_enqueue_script('custom-form-plugin-script', plugin_dir_url(__FILE__) . '/js/custom-form.js', array('jquery'), '1.0', true);
}
add_action('wp_enqueue_scripts', 'custom_form_plugin_enqueue_scripts');
function my_plugin_enqueue_styles() {
    wp_enqueue_style( 'my-plugin-styles', plugins_url( '/css/form-values.css', __FILE__ ) );
}
add_action( 'admin_enqueue_scripts', 'my_plugin_enqueue_styles' );

function custom_form_plugin_save_data(){
    global $wpdb;

    $name  = sanitize_text_field($_POST['name']);
    $email = sanitize_email($_POST['email']);
    $location  = sanitize_text_field($_POST['location']);
    $time  = sanitize_text_field($_POST['time']);
    $message  = sanitize_text_field($_POST['message']);

    $table_name = $wpdb->prefix . 'custom_form_data';
    $charset_collate = $wpdb->get_charset_collate();
    $sql = "CREATE TABLE $table_name (
        id mediumint(9) NOT NULL AUTO_INCREMENT,
        name text NOT NULL,
        email varchar(255) NOT NULL,
        location text NOT NULL,
        time text NOT NULL,
        message text NOT NULL,
        PRIMARY KEY  (id)
    ) $charset_collate;";
    require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
    dbDelta($sql);
    if (empty($name) || empty($email) || empty($location) || empty($time)) {
        echo 'All fields are required.';
        wp_die();
    }

    $wpdb->insert(
        $table_name,
        array(
            'name'    => $name,
            'email'   => $email,
            'location' => $location,
            'time'    => $time,
            'message' => $message
        )
    );

    wp_die();
}

add_action('wp_ajax_custom_form_plugin_save_data', 'custom_form_plugin_save_data');
add_action('wp_ajax_nopriv_custom_form_plugin_save_data', 'custom_form_plugin_save_data');

function custom_form_plugin_form_shortcode() {
    ob_start();
    ?>
    <form class="form form--contact" method="post">
        <div class="form__row form__group">
            <div class="form__col">
                <label>Name *</label>
                <input type="text" name="name" class="form__control" placeholder="Full Name">
            </div>
            <div class="form__col">
                <label>Email *</label>
                <input type="text" name="email" class="form__control" placeholder="example@gmail.com">
            </div>
        </div>
        <div class="form__row form__group">
            <div class="form__col">
                <label>Department *</label>
                <select name="location">
                    <option value="">Please Select</option>
                    <option value="Location-1">Location 1</option>
                    <option value="Location-2">Location 2</option>
                    <option value="Location-3">Location 3</option>
                </select>
            </div>
            <div class="form__col">
                <label>Time *</label>
                <select name="time">
                    <option value="4:00">4:00 Available</option>
                    <option value="5:00">5:00 Available</option>
                    <option value="6:00">6:00 Available</option>
                    <option value="7:00">7:00 Available</option>
                </select>
            </div>
        </div>
        <div class="form__row form__control--textarea">
            <textarea class="form__control" name="message" placeholder="Message"></textarea>
        </div>
        <div class="form__row">
            <input type="submit" value="Book Appointment" class="btn-primary">
        </div>
    </form>
    <div class="custom-form-message"></div>
    <?php
    return ob_get_clean();
}
add_shortcode('custom_form_plugin', 'custom_form_plugin_form_shortcode');

function custom_admin_menu() {
    add_menu_page('Form Values', 'Form Values', 'manage_options', 'form-values', 'display_form_values');
}
add_action('admin_menu', 'custom_admin_menu');
function display_form_values() {
    global $wpdb;
    $table_name = $wpdb->prefix . 'custom_form_data';

    $form_values = $wpdb->get_results("SELECT * FROM $table_name");
    echo '<table class="form_values">';
    echo '<thead><tr><th>Name</th><th>Email</th><th>Location</th><th>Time</th><th>Message</th></tr></thead>';
    foreach ($form_values as $value) {
        echo '<tr>';
        echo '<td>' . $value->name . '</td>';
        echo '<td>' . $value->email . '</td>';
        echo '<td>' . $value->location . '</td>';
        echo '<td>' . $value->time . '</td>';
        echo '<td>' . $value->message . '</td>';
        echo '</tr>';
    }
    echo '</table>';
}

