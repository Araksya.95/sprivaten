<?php
/**
 * We're going to configure our theme inside of a subclass of Timber\Site
 * You can move this to its own file and include here via php's include("MySite.php")
 */
class StarterSitee extends Timber\Site {
    /** Add timber support. */
    public function __construct() {
        add_action( 'after_setup_theme', array( $this, 'theme_supports' ) );
        add_filter( 'timber/context', array( $this, 'add_to_context' ) );
        add_filter( 'timber/twig', array( $this, 'add_to_twig' ) );
        add_action( 'init', array( $this, 'register_post_types' ) );
        add_action( 'init', array( $this, 'register_taxonomies' ) );
        add_action( 'init', array( $this, 'register_menus' ) );
        add_action( 'init', array( $this, 'register_widgets' ) );
        add_action( 'init', array( $this, 'register_breadcrumbs' ) );
        add_action( 'init', array( $this, 'register_subhead' ) );
        add_action( 'init', array( $this, 'acf_site_options' ) );
        parent::__construct();
    }

    /**
     * Register custom post types.
     */
    function register_post_types() {
        require 'lib/custom-post-types.php';
    }

    function acf_site_options() {
        require 'lib/acf-site-options.php';
    }

    /**
     * Register custom taxonomies.
     */
    function register_taxonomies() {
        require 'lib/taxonomies.php';
    }

    /**
     * Register custom menus.
     */
    function register_menus() {
        require 'lib/menus.php';
    }

    /**
     * Register widgets.
     */
    function register_widgets() {
        require 'lib/widgets.php';
    }

    /**
     * Register ACF site options.
     */

    /**
     * Register Breadcrumbs.
     */
    function register_breadcrumbs() {
        require 'lib/breadcrumbs.php';
    }

    /**
     * Register Subhead.
     */
    function register_subhead() {
        require 'lib/subhead.php';
    }


    /** This is where you add some context
     *
     * @param string $context context['this'] Being the Twig's {{ this }}.
     */
    public function add_to_context( $context ) {
        $context['foo'] = 'foo';
        // Menus
        $context['menus']['header_nav'] = new Timber\Menu( 'header-nav' );
        $context['menus']['footer_first']  = new Timber\Menu( 'footer-first' );
        $context['menus']['footer_second'] = new Timber\Menu( 'footer-second' );
        $context['menus']['footer_third']  = new Timber\Menu( 'footer-third' );
        $context['menus']['footer_forth']  = new Timber\Menu( 'footer-forth' );
        $context['menus']['footer_mobile']  = new Timber\Menu( 'footer-mobile' );

        $context['favicon_icon'] = get_field( 'favicon_icon', 'option' );
        $context['header'] = get_field( 'header', 'option' );
        $context['footer'] = get_field( 'footer', 'option' );
        $context['contact'] = get_field( 'contact', 'option' );
        $context['favicon'] = get_field( 'favicon', 'option' );
        $context['socials'] = get_field( 'socials', 'option' );
        $context['quiz_banner'] = get_field( 'quiz_banner', 'option' );
        $context['cta_section'] = get_field( 'cta_section', 'option' );
        $context['join_a_tour'] = get_field( 'join_a_tour', 'option' );
        $context['search_links'] = get_field( 'search_links', 'option' );

//        $context['breadcrumbs']                           = new breadcrumbs();

        $context['site']  = $this;
        return $context;
    }

    public function theme_supports() {
        // Add default posts and comments RSS feed links to head.
        add_theme_support( 'automatic-feed-links' );

        /*
         * Let WordPress manage the document title.
         * By adding theme support, we declare that this theme does not use a
         * hard-coded <title> tag in the document head, and expect WordPress to
         * provide it for us.
         */
        add_theme_support( 'title-tag' );

        /*
         * Enable support for Post Thumbnails on posts and pages.
         *
         * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
         */
        add_theme_support( 'post-thumbnails' );

        /*
         * Switch default core markup for search form, comment form, and comments
         * to output valid HTML5.
         */
        add_theme_support(
            'html5',
            array(
                'comment-form',
                'comment-list',
                'gallery',
                'caption',
            )
        );

        /*
         * Enable support for Post Formats.
         *
         * See: https://codex.wordpress.org/Post_Formats
         */
        add_theme_support(
            'post-formats',
            array(
                'aside',
                'image',
                'video',
                'quote',
                'link',
                'gallery',
                'audio',
            )
        );

        add_theme_support( 'menus' );

        function br_conditionally_load_assets(){
        if( is_front_page() ) {
            //css
            wp_dequeue_style('contact-form-7');
            wp_dequeue_style('simple-share-buttons-adder-font-awesome');
            wp_dequeue_style('ald-styles');
            wp_dequeue_style('wp-block-library');
            wp_dequeue_style('genesis-blocks-style-css');
            wp_dequeue_style('perfmatters-used');

            //scripts
            wp_dequeue_script('genesis-blocks-dismiss-js');
            wp_dequeue_script('simple-share-buttons-adder-ssba');
            wp_dequeue_script('contact-form-7');
            wp_dequeue_script('ssba-sharethis');
            wp_dequeue_script('ald-script');
        }
        }
        add_action( 'wp_enqueue_scripts', 'br_conditionally_load_assets' );
    }

    /** This Would return 'foo bar!'.
     *
     * @param string $text being 'foo', then returned 'foo bar!'.
     */
    public function myfoo( $text ) {
        $text .= ' bar!';
        return $text;
    }

    /** This is where you can add your own functions to twig.
     *
     * @param string $twig get extension.
     */
    public function add_to_twig( $twig ) {
        $twig->addExtension( new Twig\Extension\StringLoaderExtension() );
        $twig->addFilter( new Twig\TwigFilter( 'myfoo', array( $this, 'myfoo' ) ) );
        return $twig;
    }

}

new StarterSitee();
