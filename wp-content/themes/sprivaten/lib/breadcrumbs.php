<?php
/**
 * Class breadcrumbs
 */
class breadcrumbs {

    public $page_url;
    public $page_name;

    /**
    * Breadcrumbs constructor.
    */
    public function __construct(){
        $this->get_data();
    }

    /**
    * Detect breadcrumbs title and url
    */
    public function get_data(){
        global $post;

        $this->page_url = home_url('/');
        $this->page_name = 'Home';

        if (is_singular() && !empty(get_post_type_archive_link($post->post_type))){
            $post_type_obj = get_post_type_object( $post->post_type );
            $this->page_url = get_post_type_archive_link($post->post_type);
            $this->page_name = $post_type_obj->label;

        }

        if ( is_singular( 'condition' ) ) {
       
            $this->page_url = home_url('/conditions-we-treat');
            $this->page_name = 'Conditions We Treat';

        }
        

    }
}

