<?php
function register_post_type_trainers()
{
    $labels = array(
        'name' => _x('Team', 'Post type general name'),
        'singular_name' => _x('Team', 'Post type singular name'),
        'add_new' => _x('Add New', 'New post'),
        'add_new_item' => __('Add New Team'),
        'edit_item' => __('Edit Team'),
        'new_item' => __('New Team'),
        'all_items' => __('All Team'),
        'view_item' => __('View Team'),
        'search_items' => __('Search Team'),
        'not_found' => __('No Team Found'),
        'not_found_in_trash' => __('No Team Found In Trash'),
        'parent_item_colon' => ''
    );
    $args = array(
        'labels' => $labels,
        'public' => true,
        'publicly_queryable' => true,
        'show_ui' => true,
        'query_var' => true,
        'rewrite' => true,
        'capability_type' => 'post',
        'hierarchical' => true,
        'has_archive' => true,

        'menu_position' => null,
        'supports' => array('title', 'thumbnail', 'excerpt')
    );
    register_post_type('team', $args);
}

register_post_type_trainers();




