<?php

function register_taxonomy_location(){

    $labels = array(
        'name' => _x( 'Filter By Location', 'taxonomy general name' ),
        'singular_name' => _x( 'Filter By Location', 'taxonomy singular name' ),
        'search_items' =>  __( 'Search Location' ),
        'all_items' => __( 'All Locations' ),
        'parent_item' => __( 'Parent Location' ),
        'parent_item_colon' => __( 'Parent Location:' ),
        'edit_item' => __( 'Edit Location' ),
        'update_item' => __( 'Update Location' ),
        'add_new_item' => __( 'Add New Location' ),
        'new_item_name' => __( 'New Location Name' ),
        'menu_name' => __( 'Filter By Location' ),
    );

    register_taxonomy('filter_by_location', array('village'), array(
        'hierarchical' => true,
        'labels' => $labels,
        'show_ui' => true,
        'show_admin_column' => true,
        'query_var' => true,
        'rewrite' => array( 'slug' => 'filter_by_location' ),
    ));

}
register_taxonomy_location();

function register_taxonomy_event_type(){

    $labels = array(
        'name' => _x( 'Filter By Event Type', 'taxonomy general name' ),
        'singular_name' => _x( 'Filter By Event Type', 'taxonomy singular name' ),
        'search_items' =>  __( 'Search Event Type' ),
        'all_items' => __( 'All Event Types' ),
        'parent_item' => __( 'Parent Event Type' ),
        'parent_item_colon' => __( 'Parent Event Type:' ),
        'edit_item' => __( 'Edit Event Type' ),
        'update_item' => __( 'Update Event Type' ),
        'add_new_item' => __( 'Add New Event Type' ),
        'new_item_name' => __( 'New Event Type Name' ),
        'menu_name' => __( 'Filter By Event Type' ),
    );

    register_taxonomy('filter_by_event_type', array('village'), array(
        'hierarchical' => true,
        'labels' => $labels,
        'show_ui' => true,
        'show_admin_column' => true,
        'query_var' => true,
        'rewrite' => array( 'slug' => 'filter_by_event_type' ),
    ));

}
register_taxonomy_event_type();

function register_taxonomy_time(){

    $labels = array(
        'name' => _x( 'Filter By Time', 'taxonomy general name' ),
        'singular_name' => _x( 'Filter By Time', 'taxonomy singular name' ),
        'search_items' =>  __( 'Search Time' ),
        'all_items' => __( 'All Times' ),
        'parent_item' => __( 'Parent Time' ),
        'parent_item_colon' => __( 'Parent Time:' ),
        'edit_item' => __( 'Edit Time' ),
        'update_item' => __( 'Update Time' ),
        'add_new_item' => __( 'Add New Time' ),
        'new_item_name' => __( 'New Time Name' ),
        'menu_name' => __( 'Filter By Time' ),
    );

    register_taxonomy('filter_time', array('village'), array(
        'hierarchical' => true,
        'labels' => $labels,
        'show_ui' => true,
        'show_admin_column' => true,
        'query_var' => true,
        'rewrite' => array( 'slug' => 'filter_by_time' ),
    ));

}
register_taxonomy_time();


