<?php
/**
 * Class breadcrumbs
 */
class subhead {

    public $subhead;
    public $type;
    public $title;
    public $paragraph;
    public $image;
    public $has_video = false;
    public $video;

    /**
    * Breadcrumbs constructor.
    */
    public function __construct() {
        $this->get_data();
    }

    /**
     * Get page specific subhead data.
     */
    public function get_data() {

        // Get the ACF fields.
        $this->subhead = get_field('subhead');

        // Set our variables.
        $this->type = $this->type();
        $this->title = $this->title();
        $this->paragraph = $this->paragraph();
        $this->has_video = $this->has_video();
        $this->video = $this->video();
        $this->image = $this->image();
        $this->background = $this->background();

    }

    private function type() {
        if ( isset( $this->subhead['type'] ) ) {
			return $this->subhead['value'];
        }
    }

    private function title() {
        global $post;
        if ( !empty( $this->subhead['subhead_title'] ) ) {
			return $this->subhead['subhead_title'];
        } else {
            return $post->post_title;
        }
        if ( ! empty( $this->subhead['subhead_title'] ) ) {
			return $this->subhead['subhead_title'];
        } 

        if ( ! empty( $post->post_title ) ) {
            return $post->post_title;
        }

        return '';
    }

    private function paragraph() {
        if ( isset( $this->subhead['subhead_paragraph'] ) ) {
            return $this->subhead['subhead_paragraph'];
        }

        return "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas ut efficitur nisi. Integer vitae odio finibus, vulputate purus at, porta diam.";
    }

    private function image() {
        $image = '';

        // Webinars.
        if ( is_singular( 'webinars' ) ) {
            $image = get_field('webinars_background_image', 'option');
        }
        // Conditions.
                if ( is_singular( 'condition' ) ) {
                    $conditions = get_field('conditions', 'option');
                    $image = $conditions['background_image'];
                }
        if ( isset( $this->subhead['subhead_background_image'] ) && empty( $image ) ) {
			$image = $this->subhead['subhead_background_image'];
        }
        
        return $image;
    }

    private function background() {
        if ( !empty( $this->image) ) {
            return "background-image: linear-gradient(to bottom, rgba(49, 80, 114, .95), rgba(34, 44, 83, .95)), url('{$this->image}');";
        }

        return '';
    }

    private function has_video() {
        if ( isset( $this->subhead['subhead_youtube_video_id'] ) ) {
            return true;
        }
        
        return false;
    }

    private function video() {
        if ( isset( $this->subhead['subhead_youtube_video_id'] ) ) {
			return $this->subhead['subhead_youtube_video_id'];
        }
    }
}