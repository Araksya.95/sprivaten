<?php

/**
 * Register site menus.
 */
register_nav_menus(
    array(
        'header-nav' => __( 'Header - Nav' ),
        'footer-first' => __( 'Footer - First' ),
        'footer-second' => __( 'Footer - Second' ),
        'footer-third' => __( 'Footer - Third' ),
        'footer-forth' => __( 'Footer - Forth' )
    )
);

add_action( 'init', 'register_my_menus' );
