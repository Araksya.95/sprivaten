const checkWindowResize = (cb, delay) => {
    let lastWindowSize = window.innerWidth,
        timerId = null
    return (...args) => {
        const currentWindowSize = window.innerWidth
        if (currentWindowSize != lastWindowSize) {
            clearTimeout(timerId)
            timerId = setTimeout(() => {
                cb(...args)
            }, delay)
        }
        lastWindowSize = currentWindowSize
    }
}
export default class Header {
    constructor() {
        this.header = document.querySelector('.header')
        this.headerSticky = document.querySelector('.header--container')
        this.navToggle = document.querySelector('.header__toggle')
        this.nav = document.querySelector('.nav')
        this.navClose = document.querySelector('.nav__close-btn')
        this.body = document.querySelector('body')

        this.headerHeight = this.getHeaderHeight()
        this.toggleNav()
    }

    toggleNav() {
        //toggle nav click on toggle button
        this.navToggle.addEventListener('click', (e) => {
            e.preventDefault()
            this.togglefadeNav(this.nav)
        })

        //hide nav if click arrow
        this.navClose.addEventListener('click', (e) => {
            e.preventDefault()
            this.closeNav()
        })

        //hide nav if click out side of nav
        this.clickOutsideOfNav('.nav')
    }

    //sticky header
    initSticky() {
        this.updateHeaderSize()
        this.stickyOnScroll()
        window.addEventListener('scroll', this.stickyOnScroll.bind(this))
    }

    //stick header
    stickyOnScroll() {
        const scrollTop = window.scrollY

        if (scrollTop >= this.headerHeight) {
            this.body.classList.add('header--sticky')
            this.header.classList.add('header--sticky')
        } else {
            this.header.classList.remove('header--sticky')
            this.body.classList.remove('header--sticky')
        }
    }

    //calculate header height
    getHeaderHeight() {
        return this.headerSticky.offsetHeight
    }

    //update headerHeight on resize
    updateHeaderSize() {
        window.addEventListener(
            'resize',
            checkWindowResize(() => {
                this.headerHeight = this.getHeaderHeight()
            }, 500)
        )
    }

    //click out side nav
    clickOutsideOfNav(clickOutSideElement) {
        this.nav.addEventListener('click', (e) => {
            if (e.target.matches(clickOutSideElement)) {
                this.closeNav()
            }
        })
    }

    //toggle fade nav
    togglefadeNav(element) {
        if (window.getComputedStyle(element).display == 'none') {
            element.style.display = 'flex'
            setTimeout(() => {
                element.classList.add('show')
                this.header.classList.add('menu-opened')
            }, 10)
        } else {
            element.classList.remove('show')

            setTimeout(() => {
                element.style.display = 'none'
                this.header.classList.remove('menu-opened')
            }, 400)
        }
    }

    //close nav
    closeNav() {
        this.navToggle.click()
    }


}


