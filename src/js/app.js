import Header from './header';
import Home from './home';
import $ from '../../node_modules/jquery/dist/jquery';
import Player from '@vimeo/player';
let player,
	hpPlayer,
	modal = $('.vimeo-modal');

window.addEventListener('DOMContentLoaded', () => {
	const header = new Header()
	const home = new Home()
	header.initSticky()
	home.initBrandCarousel()
	$(document).on('click', '.hp-video__play', function (e) {
		e.preventDefault();

		let _this = $(this),
			getVideoId = _this.data('video-url'),
			options = {
				id: getVideoId,
				loop: true,
				color: 'ffffff'
			},
			hpPlayer = new Player('hp-video-player', options);
		hpPlayer.play();
		$('.hp-video__frame-placeholder').addClass('hp-video__frame-placeholder--hidden');

		hpPlayer.on('play', () => {
			$('.hp-video__frame-placeholder').addClass('hp-video__frame-placeholder--hidden');
		});

		hpPlayer.on('pause', () => {
			console.log('trigged');
			$('.hp-video__frame-placeholder').removeClass('hp-video__frame-placeholder--hidden');
		});
	});


})

