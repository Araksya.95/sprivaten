import Swiper, {Autoplay} from 'swiper'
import LazyLoad from 'vanilla-lazyload'

export default class Home {
    initBrandCarousel() {
        new Swiper('.brand', {
            modules: [Autoplay],
            spaceBetween: 95,
            slidesPerView: 1.5,
            roundLengths: true,
            centeredSlides: true,
            centeredSlidesBounds: true,
            loop: true,
            loopAdditionalSlides: 30,
            speed: 3000,
            autoplay: {
                delay: 1,
                disableOnInteraction: false,
            },
            breakpoints: {
                992: {
                    slidesPerView: 3,
                    spaceBetween: 30,
                },
            },
            on: {
                afterInit: (swiper) => {
                    new LazyLoad({
                        container: swiper.el,
                        cancel_on_exit: false,
                    })
                },
            },
        })
    }
}